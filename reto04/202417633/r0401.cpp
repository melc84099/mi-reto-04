/*Implemente una funci�n que permita generar un arreglo de tama�o aleatorio entre 100 y 500
y elementos aleatorios entre 1 y 10000. Incluya c�digo de prueba.*/
#include <iostream>
#include <cstdlib>
#include <ctime>


using namespace std;


int generar_num_aleatorio(int inicio, int fin) {

	srand(time(0));

	return rand() % (fin - inicio + 1) + inicio;

}

int* generar_arreglo(int& tamano) {

	tamano = generar_num_aleatorio(100, 500);

	int* arreglo = new int[tamano];

	for (int i = 0; i < tamano; ++i) {
		arreglo[i] = generar_num_aleatorio(1, 10000);
	}
	return arreglo;
}

void imprimir_arreglo(int* arreglo, int tamano) {
	cout << "Arreglo generado:" << endl;
	for (int i = 0; i < tamano; ++i) {
		cout << arreglo[i] << " ";
	}
	cout << endl;
}

int main() {

	int tamano;
	int inicio = 1;
	int fin = 10000;
	int numero_aleatorio = generar_num_aleatorio(inicio, fin);

	cout << "num aleatorio: " << numero_aleatorio << endl;

	system("pause");

	return 0;
}